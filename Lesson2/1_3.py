# Функция ​ check_numbers_by_5​ . Принимает 3 числа.
# Если ровно два из них меньше 5, то вернуть True, иначе вернуть False.

def check_numbers_by_5(a, b, c, e=0):
    if a < 5:
        e += 1
    if b < 5:
        e += 1
    if c < 5:
        e += 1
    if e == 2:
        return True
    else:
        return False


if __name__ == '__main__':
    aa = input('Введите a:')
    bb = input('Введите b:')
    cc = input('Введите c:')
    print(check_numbers_by_5(int(aa), int(bb), int(cc)))
