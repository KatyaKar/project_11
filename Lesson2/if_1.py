# Функция ​ arithmetic​ . Принимает 3 аргумента: первые 2 - числа, третий - операция,
# которая должна быть произведена над ними.
# Вернуть результат операции. Если третий аргумент +, сложить их; если —, то вычесть;
# * — умножить; / — разделить (первое на второе). В остальных случаях вернуть строку "Неизвестная операция".


def count_aba(a, b, c):
    if c == '+':
        return a + b
    elif c == '-':
        return a - b
    elif c == '*':
        return a * b
    elif c == '/':
        return a/b
    else:
        return 'Unknown'





if __name__ == '__main__':
    aa = int(input('a '))
    bb = int(input('b '))
    cc = input('c ')
    print(count_aba(aa, bb, cc))
