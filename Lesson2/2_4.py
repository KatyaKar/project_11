#Функция ​ count_user_numbers​ .​ ​ Пользователь вводит ненулевые числа до тех пор
# пока не введет ноль.
# Верните сумму этих чисел.

def count_user_numbers():
    b=0
    while True:
        a = int(input('Введите number:'))
        if a!= 0:
            b= b+a
        else:
            break
    return b

if __name__ == '__main__':
    print(count_user_numbers())