#Функция ​ count_div_12​ .​ ​ Принимает целые числа a и b.
# Вернуть количество целых чисел от a до b включительно,
# которые делятся на 12 без остатка.

def count_div_12(a,b):
    c=0
    d=0
    for i in range(a, b+1):
        c=i%12
        if c == 0:
            d=d+1
    print(d)

if __name__ == '__main__':
    aa = input('Введите a:')
    bb = input('Введите b:')
    count_div_12(int(aa),int(bb))