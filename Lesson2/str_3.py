# Функция ​ print_nth_symbols​ . Принимает строку и натуральное число n. Вывести символы с индексом n, n*2, n*3 и так далее.


def print_nth_symbols(st, n):
    i=1
    while n*i < len(st):
        print(st[n * i])
        i=i+1


if __name__ == '__main__':
    stt = input('put strok ')
    nn = int(input('n '))
    print_nth_symbols(stt, nn)
