#Функция ​ modify_str_if_begin_abc​ . Принимает строку. Если она начинается на 'abc', то заменить их на 'www',
# иначе добавить в конец строки 'zzz'. Вернуть результат.


def modify_str_if_begin_abc(st):
    if st[0:3] == 'abc':
        st=st.replace('abc', 'www')
    else:
        st = st + 'zzz'
    return st


if __name__ == '__main__':
    stt = input('put strok ')
    print(stt[0:3])
    print(modify_str_if_begin_abc(stt))