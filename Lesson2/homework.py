# Функция ​ season​ . Принимает 1 аргумент — номер месяца (от 1 до 12),
# и возвращает время года, которому этот месяц принадлежит (зима, весна, лето или осень).


def season(month):
    if (month == 12) or (month <= 2):
        return "winter"
    elif 3 <= month <= 5:
        return "spring"
    elif 6 <= month <= 8:
        return "summer"
    else:
        return "outumn"


if __name__ == '__main__':
    m = input('Введите month:')
    print(season(month = int(m)))
