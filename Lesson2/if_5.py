# Функция ​ sum_div_n​ . Принимает 3 числа. Найти сумму тех чисел, которые делятся на n. Если таких чисел нет, то вывести error


def sum_div_n(a, b, n):
    if a % n != 0:
        a = 0
    if b % n != 0:
        b = 0
    return a + b


if __name__ == '__main__':
    aa = int(input('a '))
    bb = int(input('b '))
    cc = int(input('c '))
    print(sum_div_n(aa, bb, cc))
