# Функция ​ swap​ . Принимает 2 переменных с некоторыми значениями.
# Поменять местами значения этих переменных и вывести на экран
# (например,  “a = 3 | b = True”).

def swap(a: object, b: object) -> object:
    c = a
    a = b
    b = c
    print("a=", a, "b=", b)


if __name__ == '__main__':
    aa = input('Введите a:')
    bb = input('Введите b:')
    swap(aa, bb)
