#Класс ​ Dog​ . Атрибуты ​ age​ , ​ name​ . При создании экземпляра инициализировать атрибуты класса.
# Создать метод ​ print_park​ , который печатает “Гав-гав-гав. Это мое имя ​ name​ на моем собачьем
# языке!”. Создать метод ​ get_age​ , который возвращает значение атрибута age
class MyClass():
    def __init__(self, n, a):
        self.name = n
        self.age = a

    def print_name(self):
        print('gav {} my name'.format(self.name))

    def get_age(self):
        print(self.age)


# class создаёт класс (объект-класс)
print(MyClass)
# <class '__main__.MyClass'>

# Создание экземпляра класса
# инстанцирование (англ. instantiation)
dog = MyClass('boy', '15')
dog.print_name()
dog.get_age()