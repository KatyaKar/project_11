#Класс ​ OddIntIterator​ . Аргументы: целое число ​ n​ . Перегрузить методы ​ __init__​ , ​ __iter__ ​ и ​ __next__ таким образом чтобы при итерировании по экземпляру ​
# OddIntIterator​ метод ​ next ​ возвращает следующее нечетное число в диапазоне [0, ​ n​ ), если произошел выход за пределы этого диапазона - ошибка ​ StopIteration

class Generator:
    counter = 0
    result = 0


    def __init__(self, number):
        self.number = number



    def __next__(self):
        #result = 0
        if self.counter < self.number:
            if self.counter % 2 !=0:
                result = self.counter
            else:
                pass
                #result = (self.counter if self.counter % 2 !=0 else break)
            self.counter += 1
            return result
        else:
            raise StopIteration()


    def __iter__(self):
        return self


newest = Generator(5)
print(list(newest))