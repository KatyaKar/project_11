#Создать класс ​ Zoo​ . Атрибуты ​ animal_count​ (количество животных), ​ name​ и ​ money​ (сколько денег на счету).
# При создании экземпляра инициализировать атрибуты класса. Создать метод get_animal_count​ ,
# который возвращает количество животных ​ animal_count​ в зоопарке. Создать метод ​ print_name​ ,
# который печатает строку “​ name​ - это лучший зоопарк!”. Создать метод ​ can_afford​ , который принимает аргумент needed_money и возвращает
# True если зоопарк может заплатить столько денег (self.money >= needed_money), иначе False.
class MyClass():
    def __init__(self, n, a, m):
        self.name = n
        self.qty = a
        self.money = m

    def print_name(self):
        print('gav {} my name'.format(self.name))

    def get_qty(self):
        print(self.qty)

    def can_afford(self, n_m):
        print(self.money >= n_m)



# class создаёт класс (объект-класс)
print(MyClass)
# <class '__main__.MyClass'>

# Создание экземпляра класса
# инстанцирование (англ. instantiation)
dog = MyClass('boy', '15', 100)
dog.print_name()
dog.get_qty()
dog.can_afford(50)