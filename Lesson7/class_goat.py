# Класс ​ Goat​ . Атрибуты: возраст, имя, номер загона, сколько молока дает в день. Методы:
# издать звук, напечатать имя и возраст, получить номер загона, получить сколько молока дает в день.
# Класс ​ Chicken​ . Атрибуты: имя, номер загона, сколько яиц в день. Методы: издать звук, напечатать имя,
# получить номер загона, получить сколько молока дает в день. Класс ​ Ferm​ . Атрибуты: животные
# (list из произвольного количества Goat и Chicken), наименование фермы, имя владельца фермы.
# Методы: напечатать наименование и владельца фермы, получить количество коз на ферме
# (*Подсказка isinstance или type == Goat), получить количество куриц на ферме, получить количество животных на ферме,
# получить сколько молока можно получить в день, получить сколько яиц можно получить в день
class Goat():
    def __init__(self, a, n, z, m):
        self.age = a
        self.name = n
        self.zag = z
        self.milk = m

    def print_sound(self):
        print('beeee')

    def get_name_age(self):
        print(self.name)
        print(self.age)

    def get_name_age(self):
        return(self.zag)

    def get_milk(self):
        return(self.milk)


class Chiken():
    def __init__(self, a, n, z, e):
        self.age = a
        self.name = n
        self.zag = z
        self.egg = e

    def print_sound(self):
        print('kuuuuu')

    def get_name_age(self):
        print(self.name)

    def get_name_age(self):
        return(self.zag)

    def get_egg(self):
        return(self.egg)


class Ferm:
    # Класс Книга
    # Атрибуты: страницы, название, автор, год издания
    # Методы: вывести весь контент, добавить страницу в конец,
    #         получить количество страниц, вернуть все четные страницы
    def __init__(self, title, author):
        self.animals = []
        self.title = title
        self.author = author

    def print_ferm(self):
        print(self.title)
        print(self.author)

    def extend(self, *animals):
        self.animals.extend(animals)

    def get_animals_count(self):
        return len(self.animals)

    def get_goat(self):
        a = 0
        for p in self.animals:
            if type(p) == Goat:
                a=a+1
        return a

    def get_chik(self):
        a = 0
        for p in self.animals:
            if type(p) == Chiken:
                a=a+1
        return a

    def get_goat_milk(self):
        a = 0
        for p in self.animals:
            if type(p) == Goat:
                a=a+p.milk
        return a

    def get_chik_egg(self):
        a = 0
        for p in self.animals:
            if type(p) == Chiken:
                a=a+p.egg
        return a


def main():
    # Создаем книгу и заполняем книгу страницами
    ferm = Ferm(title='ferm', author='author123')
    goat_1 = Goat(a=5, n='lll', z=12, m=5)
    goat_2 = Goat(a=2, n='mmm', z=13, m=10)
    goat_3 = Goat(a=10, n='zzz', z=14, m=6)
    ferm.extend(goat_1)
    ferm.extend(goat_2)
    ferm.extend(goat_3)
    chik_1 = Chiken(a=5, n='aaa', z=12, e=1)
    chik_2 = Chiken(a=2, n='bbb', z=13, e=2)
    ferm.extend(chik_1)
    ferm.extend(chik_2)
    ferm.print_ferm()
    print(ferm.get_animals_count())
    print(ferm.get_goat())
    print(ferm.get_chik())
    print(ferm.get_goat_milk())
    print(ferm.get_chik_egg())



if __name__ == '__main__':
    main()