#Функция ​ even_generator_til_n​ . Параметры: целое число n. Генератор возвращает все четные числа в диапазоне [0, n).
# Использовать этот генератор в цикле for и отдельно с помощью функции next (проверить, что после исчерпания элементов возвращается ошибка).
def generator(n):
    for i in range(0, n):
        if i % 2 == 0:
            yield i



myrange = generator(10)
print(list(myrange)) # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
newrange = generator(1)
print(next(newrange))  # 0 next(newrange)
next(newrange)