#Класс ​ Human​ . Атрибуты ​ age​ , ​ first_name, last_name​ . При создании экземпляра инициализировать атрибуты класса.
# Создать метод ​ get_age​ , который возвращает возраст человека. Перегрузить оператор ​ __eq__​ ,
# который сравнивает объект человека с другим по атрибутам. Перегрузить оператор __str__​ ,
# который возвращает строку в виде “Имя: ​ first_name last_name​ Возраст: ​ age​ ”. 2.
class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return 'Name:{} Age{}'.format(self.name, self.age)


dog = Dog('Rex', 2)

# __add__ и оператор (+)

# Вызов __add__ у Dog
print(dog.__str__())