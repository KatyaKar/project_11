#Функция ​ gen_dict_pow_even​ . Принимает число n. Возвращает словарь длиной n, в котором ключ -
# это четное число в диапазоне от 0 до n, а значение - квадрат ключа. Пример: n = 8, четные числа [0, 2, 4, 6],
# результат {0: 0, 2: 4, 4: 16, 6: 36}.


def gen_list_double(n):
    return {x: x ** 2 for x in range(0, n) if x % 2 == 0}


def main():
    n = int(input('put n'))
    print(gen_list_double(n))


if __name__ == '__main__':
    main()