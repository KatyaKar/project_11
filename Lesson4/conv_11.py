#Функция ​ convert_dict_to_list​ . Принимает 1 аргумент: словарь (в котором значения повторяются).
# Возвращает список ключей, список значений, количество элементов в списке ключей, количество элементов в списке значений


def get_name_by_number(names_with_numbers):
    my_list = []
    my_list2 = []
    for k, val in names_with_numbers.items():
        my_list= my_list +[k]
        my_list2= my_list2 +val

    return my_list, my_list2, len(my_list), len(my_list2)


def main():
    names_with_numbers = {'Anton': [111, 222, 444],
                          'Kate': [123, 555],
                          'Petr': [777]}
    print(get_name_by_number(names_with_numbers))



if __name__ == '__main__':
    main()