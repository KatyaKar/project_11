#Функция ​ gen_list_double​ . Принимает число n. Возвращает список длиной n, состоящий из удвоенных значений от 0 до n.
# Пример: n=3, результат [0, 2, 4].


def gen_list_double(n):
    return [x*2 for x in range(0,n)]


def main():
    n = int(input ('put n'))
    print(gen_list_double(n))


if __name__ == '__main__':
    main()