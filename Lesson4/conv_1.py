#Функция ​ convert_list_to_tuple​ . Принимает 2 аргумента: список (с повторяющимися элементами)
# и значение для поиска. Возвращает кортеж (из входного списка) и найдено ли искомое значение(True | False)


def convert_list_to_tuple(my_list, val):
    val_in = True if val in my_list else False

    return tuple(my_list), val_in


def main():
    my_list = [1, 0, 3, 2, 3, 'my']
    print(my_list)
    print(convert_list_to_tuple(my_list, 3))
    print(convert_list_to_tuple(my_list, 'my'))


if __name__ == '__main__':
    main()