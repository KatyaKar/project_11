#Функция ​ gen_dict_double​ . Принимает число n. Возвращает словарь длиной n, в котором ключ - это значение от 0 до n,
# а значение - удвоенное значение ключа. Пример: n=3, результат {0: 0, 1: 2, 2: 4}
#dc_1 = {n: n ** 2 for n in range(5)}
#print(dc_1)
# {0: 0, 1: 1, 2: 4, 3: 9, 4: 16}


def gen_list_double(n):
    return {x: x * 2 for x in range(0, n)}


def main():
    n = int(input('put n'))
    print(gen_list_double(n))


if __name__ == '__main__':
    main()
