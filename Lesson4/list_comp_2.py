# Функция ​ gen_list_pow_even​ . Принимает число n. Возвращает список длиной n,
# состоящий из квадратов четных чисел в диапазоне от 0 до n. Пример: n = 8, четные числа [0, 2, 4, 6], результат [0, 4, 16, 36].


def gen_list_double(n):
    return [x ** 2 for x in range(0, n) if x % 2 == 0]


def main():
    n = int(input('put n'))
    print(gen_list_double(n))


if __name__ == '__main__':
    main()
