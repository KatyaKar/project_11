# Определить функцию ​ flower_with_default_vals​ .
# Принимает 3 аргумента: цветок (по умолчанию “ромашка”), цвет (по умолчанию “белый”) и цена (по умолчанию 10.25).
# Функция flower_with_default_vals​ выводит строку в формате “Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>”.
# При этом в функции ​ flower_with_default_vals​ есть проверка на то, что цветок и цвет - это строки
# (* Подсказка: type(x) == str или isinstance(s, str)),
# а также цена - это число больше 0, но меньше 10000.
# В функции main вызвать функцию ​ flower_with_default_vals​ различными способами
# (перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).
# (* Использовать именованные аргументы).

isinstance
def flower_with_default_vals(fname='romashka', color='white', price=10.25):
    # if type(fname) == str and type(lname) == str and (type(age) == int and 0 < age < 150):
    if isinstance(fname, str) and isinstance(color, str) and (isinstance(price, float) and 0 < price < 10000):
        return f"Цветок:  {fname} | Цвет:{color} | Цена: {price}"

    return None



def main():
    # правильные
    print(flower_with_default_vals())
    print(flower_with_default_vals('rose'))
    print(flower_with_default_vals(color='black'))
    print(flower_with_default_vals(price=float(99.99)))
    print(flower_with_default_vals('tulp', 'blue'))
    print(flower_with_default_vals('orch', price=38.00))
    print(flower_with_default_vals(color='red', price=67.00))
    print(flower_with_default_vals('Kate', 'Katerinova', price=20.00))



if __name__ == '__main__':
    main()