#Определить функцию ​ print_student_marks_by_subject​ .
# Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (​ **kwargs).
# Выводит на экран имя студента, а затем предмет и оценку (может быть несколько,
# если курс состоял из нескольких частей).
# Пример вызова функции ​ print_student_marks_by_subject(“Вася”, math=5, biology=(3, 4), magic=(4, 5, 5)).
def print_student(name, **mark):
    print(f"stu Name: {name}")
    for sub, mar in mark.items():
        print(f"{sub}= {mar}")


print_student("Вася", math=5, biology=(3, 4), magic=(4, 5, 5))