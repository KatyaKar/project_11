# Функция ​ list_if_found​ . Принимает 3 аргумента: список “ассорти” my_list
# (который создал пользователь), число num и строку word. Возвращает “I found {word}”, # если найдена только строка word;
# “I found {num}”, если найдено только число num;
# “I found {word} and {num}!!!”, если найдены оба; “Sorry, I can’t find anything...”, если не найдено ни слово ни число.  (* тип num и word важен).


def list_count_word_num(my_list):
    i=0
    d=0
    v=0
    while i< len(my_list):
        if my_list[i].isdigit():
            d += 1
        else:
            v += 1
        i += 1
    if d!= 1 and v!=0:
        print ('I found {word} and {num}!!!')
    elif d != 1 and v==0:
        print ('I found {num}')


def main(lst_str=None):
    for i in range (0,1):
        lst_str[i] = input(f'Введите элемент {i}: ')
    list_count_word_num(lst_str)


if __name__ == '__main__':
    main()