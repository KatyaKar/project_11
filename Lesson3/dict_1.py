# Функция ​ get_translation_by_word​ .
# Принимает 2 аргумента: ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...]
# и слово для поиска в словаре (ru). Возвращает все варианты переводов, если такое слово есть в словаре,
# если нет, то ‘Can’t find Russian word: {word}’.


def get_numbers_by_name(names_with_numbers, name):
    if names_with_numbers.get(name):
        print (names_with_numbers.get(name))
    else:
        print (f'Can’t find Russian word', name)


def main():
    names_with_numbers = {'Рука': ['hand', 'arm'],
                          'Лицо': ['face'],
                          'Смотреть': ['see', 'watch']}
    get_numbers_by_name(names_with_numbers, 'Рука')
    get_numbers_by_name(names_with_numbers, 'Лицо')
    get_numbers_by_name(names_with_numbers, '??')


if __name__ == '__main__':
    main()