#Функция ​ list_count_word_num​ . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
# Возвращает количество чисел и количество слов(букв) в списке my_list.


def list_count_word_num(my_list):
    i=0
    d=0
    v=0
    while i< len(my_list):
        if my_list[i].isdigit():
            d += 1
        else:
            v += 1
        i += 1
    return d,v


def main():
    lst_str = input('Введите список через probel: ')
    lst = lst_str.split(' ')
    print(list_count_word_num(lst))


if __name__ == '__main__':
    main()