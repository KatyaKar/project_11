#Функция ​ get_words_by_translation​ .  ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...]
# и слово для поиска в словаре (eng). Возвращает список всех вариантов переводов (ru),
# если такое слово есть в словаре (eng), если нет, то ‘Can’t find English word: {word}’.


def get_numbers_by_en_word(ru_with_en, word):
    i=0
    for k, val in ru_with_en.items():
        if word in val:
            print (k)
            print (val)
            i=+1
    if i==0:
        print(f'Cant find English word: {word}')


def main():
    ru_with_en = {'Рука': ['hand', 'arm'],
                          'Лицо': ['face'],
                          'Смотреть': ['see', 'watch', 'face']}
    #print(get_numbers_by_en_word(ru_with_en, 'arm'))
    print (ru_with_en)
    print(get_numbers_by_en_word(ru_with_en, 'face'))
    #print(get_numbers_by_en_word(ru_with_en, 'watch'))
    print(get_numbers_by_en_word(ru_with_en, '??'))


if __name__ == '__main__':
    main()