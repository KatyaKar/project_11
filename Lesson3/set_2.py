#Функция ​ set_print_info_for_3​ . Принимает 3 аргумента:
# множества “ассорти” my_set_left, my_set_mid и my_set_right (которые создал пользователь).
# Выводит информацию о: равенстве множеств, имеют ли они общие элементы, являются ли они подмножествами друг друга.


def set_print_info_for_3(my_set_left, my_set_mid, my_set_right):
    print(f'left: {my_set_left} |mid: {my_set_mid}| right: {my_set_right}')
    print('равенство: {eq}'.format(eq=my_set_left == my_set_right == my_set_mid))
    print('общие элементы: {inter}'.format(inter=my_set_mid.intersection(my_set_left.intersection(my_set_right))))
    print('left подмножество right: {sub}'.format(sub=my_set_left.issubset(my_set_right)))
    print('right подмножество left: {sub}'.format(sub=my_set_right.issubset(my_set_left)))
    print('mid подмножество left: {sub}'.format(sub=my_set_mid.issubset(my_set_left)))
    print('mid подмножество right: {sub}'.format(sub=my_set_mid.issubset(my_set_right)))


def main():
    str1 = input('Введите элементы множества через пробелы: ')
    str2 = input('Введите элементы множества через пробелы: ')
    str3 = input('Введите элементы множества через пробелы: ')
    set1 = set(str1.split(' '))
    set2 = set(str2.split(' '))
    set3 = set(str3.split(' '))
    set_print_info_for_3(set1, set2, set3)


if __name__ == '__main__':
    main()
