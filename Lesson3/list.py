#Функция ​ list_count_word​ . Принимает 2 аргумента: список слов my_list (который создал пользователь) и строку word.
#Возвращает количество word в списке my_list


def list_count_num(my_list, word):
    return my_list.count(word)


def main():
    lst_str = input('Введите список через probel (без пробелов): ')
    lst = lst_str.split(' ')
    word = input('Введите слово: ')
    print(list_count_num(lst, word))


if __name__ == '__main__':
    main()